package autotest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestThree extends ConstantMethods {

    File file = new File("aboutWork.txt");

    Date date = new Date();
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("'Дата' E dd.MM.yyyy\n'Время' hh:mm:ss");

    @Test
    public void alfaBankWeb() {

        driver.get("https://www.yandex.ru/");

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        String searchSystemName = "Название поисковой системы - " + driver.getTitle();
        driver.findElement(By.xpath("/html/body//div//input[@class ='input__control input__input']")).sendKeys("Альфа банк");
        driver.findElement(By.xpath("/html/body//div[@class ='search2__button']")).click();

        try {
            WebElement dynamicElement = (new WebDriverWait(driver, 10))
                    .until(ExpectedConditions.presenceOfElementLocated(By.linkText("AlfaBank.ru")));
            dynamicElement.click();

        } catch (Exception e) {
            System.out.println("Ссылка не найдена");
        }

        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));

        JavascriptExecutor js1e = (JavascriptExecutor) driver;
        js1e.executeScript("window.scrollTo(0, document.body.scrollHeight)");

        driver.findElement(By.linkText("Вакансии")).click();
        driver.findElement(By.xpath("//body//div//nav//a[@href ='/about/']")).click();
        js1e.executeScript("window.scrollTo(0, document.body.scrollHeight)");

        Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();

        ArrayList<String> alfaBankForEmplyees = new ArrayList<String>();
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='message']")).getText());
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='info']/p")).getText());
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='info']/p[2]")).getText());
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='info']/ul")).getText());
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='info']/p[3]")).getText());
        alfaBankForEmplyees.add(driver.findElement(By.xpath("//body//div[@class='info']/ul[2]")).getText());
        alfaBankForEmplyees.add("Браузер - " + cap.getBrowserName() + "\n" + "Версия : " + cap.getVersion());
        alfaBankForEmplyees.add(simpleDateFormat.format(date));
        alfaBankForEmplyees.add(searchSystemName);

        try {
            PrintWriter fileWriter = new PrintWriter(new FileWriter(file, true));

            for (String alfaBankForEmplyee : alfaBankForEmplyees) {
                fileWriter.println(alfaBankForEmplyee);
                fileWriter.println("");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
