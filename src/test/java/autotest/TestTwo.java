package autotest;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class TestTwo extends ConstantMethods {
    @Test
    public void samsungAndBeatsTest() {
        driver.get("https://www.yandex.ru/");

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.findElement(By.linkText("Маркет")).click();
        driver.findElement(By.linkText("Электроника")).click();
        driver.findElement(By.linkText("Мобильные телефоны")).click();

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("scroll(0, 250);");


        driver.findElement(By.linkText("Samsung")).click();
        driver.findElement(By.name("Цена от")).sendKeys("40000");


        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement title = driver.findElement(By.xpath("//img[@class='image']"));//
        String firstPhoneTitle = title.getAttribute("title");
        System.out.println("Название первого телефона на странице  - " + firstPhoneTitle);
        title.click();

        String pagePhoneTitle = driver.findElement(By.xpath("//body//div//h1[@class='title title_size_28 title_bold_yes']")).getText();
        System.out.println("Название телефона на странице - " + pagePhoneTitle);

        if (firstPhoneTitle.equals(pagePhoneTitle)) {
            System.out.println("Названия телефонов совпадают!");
        } else {
            System.out.println("Названия телефонов не совпадают!");
        }

        ((JavascriptExecutor) driver).executeScript("window.open()");
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        driver.get("https://www.yandex.ru/");

        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        driver.findElement(By.linkText("Маркет")).click();
        driver.findElement(By.linkText("Электроника")).click();
        driver.findElement(By.linkText("Ещё")).click();
        driver.findElement(By.linkText("Наушники и Bluetooth-гарнитуры")).click();
        driver.findElement(By.linkText("Beats")).click();
        driver.findElement(By.name("Цена от")).sendKeys("17000");
        driver.findElement(By.name("Цена до")).sendKeys("25000");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        WebElement headPhones = driver.findElement(By.xpath("//img[@class='image']"));//
        String firstHeadPhoneTitle = headPhones.getAttribute("title");
        System.out.println("Название первых наушников на странице  - " + firstHeadPhoneTitle);
        headPhones.click();

        String pageHeadPhoneTitle = driver.findElement(By.xpath("//body//div//h1[@class='title title_size_28 title_bold_yes']")).getText();
        System.out.println("Название наушников на странице - " + pageHeadPhoneTitle);

        if (firstHeadPhoneTitle.equals(pageHeadPhoneTitle)) {
            System.out.println("Имена наушников совпадают!");
        } else {
            System.out.println("Имена наушников не совпадают!");
        }
    }
}
