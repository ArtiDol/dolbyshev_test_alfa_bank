package programm;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class TestOne {
    public static void main(String[] args) {
        try {
            FileInputStream fileInputStream = new FileInputStream("../Test/DigitArray.txt");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(fileInputStream));
            StringBuilder stringBuilder = new StringBuilder(bufferedReader.readLine());

            ArrayList<Integer> digit = new ArrayList<>();
            for (String q : stringBuilder.toString().split(",")) {
                digit.add(Integer.valueOf(q));
            }

            Collections.sort(digit);
            Stream.of(digit).forEach(System.out::print);

            System.out.println(" ");
            Collections.reverse(digit);

            Stream.of(digit).forEach(System.out::print);

        } catch (IOException e) {
            System.out.println("Файл не найден");
        }
    }
}
